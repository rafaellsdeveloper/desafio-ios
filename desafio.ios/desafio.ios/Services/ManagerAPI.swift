//
//  ManagerAPI.swift
//  desafio.ios
//
//  Created by Marcos Barbosa on 22/09/2018.
//  Copyright © 2018 n/a. All rights reserved.
//

import Foundation
import Alamofire

class ManagerAPI {
    
    
    internal func conectAPI(from url: String, completionHandler: @escaping (Bool, AnyObject?) -> Void){
        
        
        Alamofire.request(url).responseJSON { (response) in
            
            if response.value != nil {
                completionHandler(true, response.value as AnyObject)
            }else{
                completionHandler(false, nil)
            }
        
        }
        
    }
    
}
