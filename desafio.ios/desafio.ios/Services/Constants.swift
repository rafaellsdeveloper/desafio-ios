//
//  Constants.swift
//  desafio.ios
//
//  Created by Marcos Barbosa on 22/09/2018.
//  Copyright © 2018 n/a. All rights reserved.
//

import Foundation

let FILMS = "https://api.themoviedb.org/3/movie/popular?api_key=8d797a8a4596f5007e4d2833ae681174&language=en-US&page=1"
let FILMS_GENERS = "https://api.themoviedb.org/3/genre/movie/list?api_key=8d797a8a4596f5007e4d2833ae681174&language=en-US"
let IMAGES = "https://image.tmdb.org/t/p/w500/"
