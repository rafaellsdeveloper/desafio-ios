//
//  DetailFilmsViewController.swift
//  desafio.ios
//
//  Created by Marcos Barbosa on 22/09/2018.
//  Copyright (c) 2018 n/a. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DetailFilmsDisplayLogic: class
{
    func displaySomething(viewModel: [DetailFilms.Something.ViewModelGeners])
}

class DetailFilmsViewController: UIViewController, DetailFilmsDisplayLogic {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    @IBOutlet weak var GenersLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    
    var interactor: DetailFilmsBusinessLogic?
    var router: (NSObjectProtocol & DetailFilmsRoutingLogic & DetailFilmsDataPassing)?
    var geners = [DetailFilms.Something.ViewModelGeners]()
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = DetailFilmsInteractor()
        let presenter = DetailFilmsPresenter()
        let router = DetailFilmsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        doSomething()
        configureView(data: (router?.dataStore?.film)!)
        
    }
    
    func configureView(data: DetailFilms.Something.ViewModel){
        
        if let title = data.title as? String {
            //self.navigationController?.navigationBar.topItem?.title = title
            self.navigationItem.title = title
            self.titleLabel.text = title
        }
        
        if let image = data.posterPath as? String {
            
            let url : NSURL? = NSURL(string: "\(IMAGES)\(image)")
            if let urlImage = url{
                backgroundImage.sd_setImage(with: urlImage as URL, completed: nil)
                posterImage.sd_setImage(with: urlImage as URL, completed: nil)
                
                posterImage.layer.shadowOffset = CGSize(width: 0, height: 3)
                posterImage.layer.shadowColor = UIColor.black.cgColor
                posterImage.layer.shadowRadius = 6.0
                posterImage.layer.shadowOpacity = 1.0
            }
            
        }
        
        if let release = data.release as? String {
            let date = release.split(separator: "-")
            self.releaseLabel.text = "\(date[2])-\(date[1])-\(date[0])"
        }
        
        if let overview = data.overview as? String {
            self.overviewLabel.text = overview
        }
        
    }
    
    func doSomething()
    {
        let request = DetailFilms.Something.Request()
        interactor?.doSomething(request: request)
    }
    
    func displaySomething(viewModel: [DetailFilms.Something.ViewModelGeners]) {
        
        geners = viewModel
        
        if let geners = router?.dataStore?.film.genreIds as? NSArray{
            
            var genersSelected = [String]()

            for i in viewModel {
                
                for f in geners {
                    
                    if i.id as! Int == f as! Int{
                        if genersSelected.contains(where: {$0 == i.name as! String}){
                            
                        }else{
                            genersSelected.append(i.name as! String)
                        }
                    }
                    
                }
                
            }
            
            var items = ""
            if genersSelected.count > 0 {
                
                for i in genersSelected {
                    items += "\(i)"
                    
                    if (i == genersSelected.last) {
                        items += "."
                    }else{
                        items += ", "
                    }
                }
            }
            
            GenersLabel.text = items
            
            
        }
    }
}
