//
//  HomeCollectionViewCell.swift
//  desafio.ios
//
//  Created by Marcos Barbosa on 22/09/2018.
//  Copyright © 2018 n/a. All rights reserved.
//

import UIKit
import SDWebImage

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filmImage: UIImageView!
    @IBOutlet weak var filmLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    func configureCell(with data: [Home.Something.ViewModel], and indexPath: IndexPath) {
        
        if data.count > 0{
            
            filmLabel.text = data[indexPath.row].title
            
            if let image = data[indexPath.row].posterPath as? String{
                let url : NSURL? = NSURL(string: "\(IMAGES)\(image)")
                if let urlImage = url{
                    filmImage.sd_setImage(with: urlImage as URL, completed: nil)
                }
            }
            
            self.contentView.layer.shadowOffset = CGSize(width: 2, height: 2)
            self.contentView.layer.shadowColor = UIColor.black.cgColor
            self.contentView.layer.shadowRadius = 4
            self.contentView.layer.shadowOpacity = 1.0
            
            self.favoriteButton.layer.cornerRadius = self.favoriteButton.frame.height / 2
        }
        
    }
    
}
